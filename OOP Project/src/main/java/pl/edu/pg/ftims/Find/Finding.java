package main.java.pl.edu.pg.ftims.Find;

import main.java.pl.edu.pg.ftims.PException.*;

public interface Finding {
	public void find(String name) throws NoProductException;
}
