package main.java.pl.edu.pg.ftims.Data;

public class Product extends Category {
private String name;
private double value;
private String producer;

//get
public String getName(){
	return this.name;
}
public double getValue(){
	return this.value;
}
public String getCategory(){
	return catName;
}
public String getProducer(){
	return producer;
}

//set
public void setName(String name){
	this.name = name;
}
public void setValue(double value){
	this.value = value;
}
public void setCategory(String catName){
	this.catName = catName;
}
public void setProducer(String producer){
	this.producer = producer;
}

@Override
public String toString(){
	return name + ", " + producer + ", " + value + " PLN";
}

//Constructors
public Product(){}
public Product(String name, String catName, String producer , double value){
	this.name = name;
	this.catName = catName;
	this.value = value;
	this.producer=producer;
	
}
}
