package main.java.pl.edu.pg.ftims.showall;

import java.util.LinkedList;
import java.util.Map;

import main.java.pl.edu.pg.ftims.Data.*;

public class ShowAll implements Showing {
	private Map<String,LinkedList<Product>> all;
	
	@Override
	public void show(){
		for (Map.Entry<String,LinkedList<Product>> category : all.entrySet()) {
			System.out.println("Category " + category.getKey());
			for(Product product : category.getValue()){
				System.out.println("\t" + product.getName());
			}
			
		}
	}
	
	public ShowAll(Map<String,LinkedList<Product>> all){
		this.all = all;
	}
}
