package main.java.pl.edu.pg.ftims.PException;

public class NoProductException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage(){
		return "No product found";
	}
	
	@Override
	public String toString(){
	return "No product found";
}
}
