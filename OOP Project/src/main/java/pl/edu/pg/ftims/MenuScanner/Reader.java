package main.java.pl.edu.pg.ftims.MenuScanner;

import java.util.Scanner;
import main.java.pl.edu.pg.ftims.showall.*;

public class Reader {
	private static Scanner scanner;
	
	public static Integer readInt(Showing menu) {
		Integer catchedInt = null;
		do{
			scanner = new Scanner(System.in);
			menu.show();
		
			try{
			catchedInt = scanner.nextInt(); 
			} catch (Exception e) {
				menu.show();
				catchedInt = null;
			}
		} while(catchedInt == null);
		return catchedInt;
	}
	
	public static String readString(Showing menu) {
		String catchedString = null;
		do{
			scanner = new Scanner(System.in);
			menu.show();
		
			try{
			catchedString = scanner.nextLine(); 
			} catch (Exception e) {
				menu.show();
				catchedString = null;
			}
		} while(catchedString == null);
		return catchedString;
	}
	
	public static void closeScanner() {
		scanner.close();
	}

}
