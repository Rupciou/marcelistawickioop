package main.java.pl.edu.pg.ftims.Find;

import java.util.LinkedList;
import java.util.Map;

import main.java.pl.edu.pg.ftims.Data.*;
import main.java.pl.edu.pg.ftims.PException.*;
import main.java.pl.edu.pg.ftims.showall.*;

public class FindProduct implements Showing, Finding{
	private Map<String,LinkedList<Product>> all;
	private Product product;
	
	@Override
	public void find(String name) throws NoProductException {
		for (Map.Entry<String,LinkedList<Product>> category : all.entrySet()) {
			for(Product product : category.getValue()){
				if (product.getName().equals(name)) {
					this.product = product;
					show();
					return;
				}
			}	
		}
		
		throw new NoProductException();
	}
	
	@Override
	public void show(){
		System.out.println(product);
	}
	
	public FindProduct(Map<String,LinkedList<Product>> all){
		this.all = all;
	}
}
