package main.java.pl.edu.pg.ftims;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import main.java.pl.edu.pg.ftims.Data.*;
import main.java.pl.edu.pg.ftims.Find.*;
import main.java.pl.edu.pg.ftims.MenuScanner.*;
import main.java.pl.edu.pg.ftims.PException.*;
import main.java.pl.edu.pg.ftims.showall.*;

public class MainClass {

	public static void main(String[] args) {
		List<Product> myProducts = new LinkedList<Product>();
		myProducts.add(new Product("Mirror", "Bathroom", "AProducts", 120.00));
		myProducts.add(new Product("Flush", "Bathroom", "Kova", 220.00));
		myProducts.add(new Product("Sink", "Bathroom", "Kova", 320.00));
		myProducts.add(new Product("Bath", "Bathroom", "Kova", 420.00));
		myProducts.add(new Product("Table", "Kitchen", "Atlanta", 100));
		myProducts.add(new Product("Chair", "Kitchen", "Atlanta", 120));
		myProducts.add(new Product("Fridge", "Kitchen", "Bosh", 110));
		myProducts.add(new Product("TV", "Livingroom", "Samsung", 1900));
		myProducts.add(new Product("Table", "Livingroom", "Atlanta", 190));
		myProducts.add(new Product("Bed", "Livingroom", "Kjerbie", 1100));
		Map<String,LinkedList<Product>> all = new HashMap<String,LinkedList<Product>>();
	
		for (Product product : myProducts) {
			if (all.get(product.getCategory()) == null) {
				all.put(product.getCategory(), new LinkedList<Product>());
				all.get(product.getCategory()).add(product);
			}
			else all.get(product.getCategory()).add(product);
		}
		
		Integer chose = null;
		ShowAll pokaz = new ShowAll(all);
		FindProduct findProduct = new FindProduct(all);
	
		Boolean done = false;
		do {
			chose = Reader.readInt(new Menu("1 - Show all\n2 - Find by name\n3 - Exit"));
			
			switch(chose) {
				case 1: pokaz.show();
				break;
				case 2: try {
					findProduct.find(Reader.readString(new Menu("Product to find: ")));
				} catch (NoProductException e) {
					System.out.println(e);
				}
				break;
				case 3: done = true;
		}
		} while(!done);
		
		Reader.closeScanner();
	}
}